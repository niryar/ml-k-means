#Nir Yarkoni
#313587057
from init_centroids import init_centroids
import numpy as np


from scipy.misc import imread


# k is the number of cluster
def divide_to_cluster(k, x, centroids):
    # create k lists
    d = []

    list_number = 0
    while list_number < k:
        d.append([])
        list_number += 1

    for vectorRGB in x:
        # loop = 0
        diff = diff_to_centroid(centroids[0] - vectorRGB)
        index_cluster = 0
        # for cen_i in centroids:
        for j in range(0, len(centroids)):
            if diff >= diff_to_centroid(centroids[j] - vectorRGB):
                diff = diff_to_centroid(centroids[j] - vectorRGB)
                index_cluster = j

        d[index_cluster].append(vectorRGB)
    return d


def diff_to_centroid(value):
    diff = np.linalg.norm(value)
    return diff


def average_of_centroid(lists_of_list, K, centroids):
    for iCluster in range(0, K):
        sum_cluster = np.sum(lists_of_list[iCluster], axis=0)
        size_cluster = len(lists_of_list[iCluster])
        centroids[iCluster] = [val / size_cluster for val in sum_cluster]
    return centroids


def print_centroids(centroids, K, index):
    print("iter %d: " % (index), end='')
    for i in range(0, K):
        print(
            '[%s, %s, %s]' % (cat_after_point(centroids[i][0]), cat_after_point(centroids[i][1]),
                              cat_after_point(centroids[i][2])),
            end='')

        if i != K - 1:
            print(", ", end='')
    print("")


def set_colors(x, centroids):
    for vectorRGB in range(0, len(x)):
        diff = diff_to_centroid(centroids[0] - x[vectorRGB])
        for j in range(0, len(centroids)):
            if diff >= diff_to_centroid(centroids[j] - x[vectorRGB]):
                diff = diff_to_centroid(centroids[j] - x[vectorRGB])
                index_cluster = j
        # put the vectorRGB at list at index cluster_num
        x[vectorRGB][0] = centroids[index_cluster][0]
        x[vectorRGB][1] = centroids[index_cluster][1]
        x[vectorRGB][2] = centroids[index_cluster][2]


def cat_after_point(f):
    value =np.floor(f * 100) / 100
    if value== 0:
        return "0."
    return value


def k_means():
    iter_num = 11
    K_list = [2, 4, 8, 16]

    for i in range(0, len(K_list)):

        path = 'dog.jpeg'
        A = imread(path)
        A_norm = A.astype(float) / 255.
        img_size = A_norm.shape
        X = A_norm.reshape(img_size[0] * img_size[1], img_size[2])

        K = K_list[i]
        print("k=" + K.__str__() + ":")

        centroids = init_centroids(X, K)
        print_centroids(centroids, K, 0)
        for index in range(1, iter_num):
            # print("iter", index, ":")
            list_of_cluster = divide_to_cluster(K, X, centroids)
            centroids = average_of_centroid(list_of_cluster, K, centroids)
            print_centroids(centroids, K, index)

        set_colors(X, centroids)

if __name__ == '__main__':
    k_means()

